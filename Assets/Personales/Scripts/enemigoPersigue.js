#pragma strict

var Player : Transform;
var damp = 2;
var velocidadePerseguidor = 4;
//var projectile : Rigidbody;
var speed = 20;
var distanciaParaPerseguir = 70;
var flameDestroy : GameObject;
var flameDestroyEnemigo : GameObject;

var vidaEnemigo : float = 200.0 ;

private var obrero : stadisticsPlayer;


function Update(){
	
	if(Player!=null){
		
		if(Vector3.Distance(Player.position,transform.position) < distanciaParaPerseguir){
			// aqui podes poner animaciones
			var rotate = Quaternion.LookRotation(Player.position - transform.position);
			
			transform.rotation = Quaternion.Slerp(transform.rotation, rotate,damp * Time.deltaTime);
			
			transform.Translate(0,0, velocidadePerseguidor * Time.deltaTime);
			
			// var instantiatedProjectile : Rigidbody = Instantiate(projectile, transform.position, transform.rotation );
			//instantiatedProjectile.velocity =  transform.TransformDirection( Vector3( 0, 0, speed ) );
			//Physics.IgnoreCollision( instantiatedProjectile.GetComponent(collider), transform.root.GetComponent(collider) );
		}
		if(Vector3.Distance(Player.position,transform.position) < 2){
			//aqui podes poner animaciones
			//animation.wrapMode =WrapMode.Once;
		}
	}
	if(vidaEnemigo <= 0){
		startDestroyThis();
	}
}


function Start (){
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);
	flameDestroy.SetActive(false);
	flameDestroyEnemigo.SetActive(false);
}

function OnTriggerStay(player: Collider){
	
	if(player.tag=="Player"){
		
		if(vidaEnemigo > 0){
			startDestroy();
				}
			
		}
		
	}
	
	
	function startDestroy(){
		
		flameDestroy.SetActive(true);
		yield WaitForSeconds(2);
		Destroy(gameObject);
		obrero.vidaActuales -= 2;
		flameDestroy.SetActive(false);
	}
	
	function startDestroyThis(){
		
		flameDestroyEnemigo.SetActive(true);
		yield WaitForSeconds(2);
		Destroy(gameObject);
		flameDestroyEnemigo.SetActive(false);
	}
	
	function OnGUI()
	{
		var name : int = 0 ;
		if(this.name == "1"){
			name = 0;
		}else if(this.name == "2"){
			name = 1;
		}else if(this.name == "3"){
			name = 2;
		}else if(this.name == "4"){
			name = 3;
		}else if(this.name == "5"){
			name = 4;
		}
		
		GUI.Box(new Rect(5, 300 + (25 * name), 120, 23), "Fantasma " + this.name + ": " + vidaEnemigo.ToString("0") );
		
	}