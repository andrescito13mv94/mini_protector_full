#pragma strict
var Pregunta1: GameObject;
var Pregunta2: GameObject;
var Pregunta3: GameObject;

var pregunta1_habilitada: boolean = true;
var pregunta2_habilitada: boolean = false;
var pregunta3_habilitada: boolean = false;

private var obrero : stadisticsPlayer;
private var puertaBanio : RotarPuerta;
private var moverCamion : MoverCamion;

var preguntasBuenas : int = 0;
var inTrigger : boolean = false;

var mensajeBadNews : boolean = false;
var mensajeGoodNews : boolean = false;
var mensaje_espera : boolean = false;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);
	puertaBanio = GameObject.Find("Doorsbanio").GetComponent(RotarPuerta);
	moverCamion = GameObject.Find("Old_Truck").GetComponent(MoverCamion);
	Pregunta1.SetActive(false);
	Pregunta2.SetActive(false);
}

function Update () {
	if(inTrigger) {
		if(pregunta1_habilitada){
			if(Input.GetKeyDown("4") ){
					preguntasBuenas += 1;
					
				Pregunta1.SetActive(false);
				pregunta1_habilitada=false;
					mensaje_espera = true;
				
					mensaje_espera = false;
				pregunta2_habilitada=true;
				Pregunta2.SetActive(true);
				
			}
			
			
		}
		if(pregunta2_habilitada){
			if(Input.GetKeyDown("5") ){
					preguntasBuenas += 1;
					
				pregunta1_habilitada=false;
				pregunta2_habilitada=false;
					mensaje_espera = true;				 
					mensaje_espera = false;
				pregunta3_habilitada=true;
				Pregunta2.SetActive(false);	
				Pregunta3.SetActive(true);	
				
			}
			
		}
		
		if(pregunta3_habilitada){
			
				if(Input.GetKeyDown("0") ){
					preguntasBuenas += 1;
				
				pregunta1_habilitada=false;
				pregunta2_habilitada=false;
				pregunta3_habilitada=false;
				Pregunta3.SetActive(false);	
				
				if(preguntasBuenas < 2){
					agregarBajo();
					perder();		
			
				}else{	
				
					ganar();
				
				}
				
			}
			
			
			
			
		}
		
	}
	
}

function perder(){
				mensajeBadNews = true;
	            yield WaitForSeconds(3);
				mensajeBadNews = false;
				pregunta1_habilitada=true;
				pregunta2_habilitada=false;
				pregunta3_habilitada=false;
	
}

function ganar(){
				puertaBanio.enabled=true;
				obrero.max_pistas = 6;
				mensajeGoodNews = true;
				 yield WaitForSeconds(3);
				mensajeGoodNews = false;
				moverCamion.enabled=true;
}

function OnTriggerStay(player: Collider){
	if(player.tag=="Player"){ 
		inTrigger = true;
		
		if(pregunta1_habilitada){
			Pregunta1.SetActive(true);
			
			if(Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3") || Input.GetKeyDown("5") || Input.GetKeyDown("6") || Input.GetKeyDown("7") || Input.GetKeyDown("8") || Input.GetKeyDown("9") || Input.GetKeyDown("0")){
				Pregunta1.SetActive(false);
				pregunta1_habilitada=false;
					mensaje_espera = true;
				yield WaitForSeconds(1);
					mensaje_espera = false;
				pregunta2_habilitada=true;
				Pregunta2.SetActive(true);
				
			}
			
		}
		if(pregunta2_habilitada){
			Pregunta2.SetActive(true);
			
			 
			if(Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3") || Input.GetKeyDown("4") || Input.GetKeyDown("6") || Input.GetKeyDown("7") || Input.GetKeyDown("8") || Input.GetKeyDown("9") || Input.GetKeyDown("0")){
				pregunta1_habilitada=false;
				pregunta2_habilitada=false;
					mensaje_espera = true;
				 yield WaitForSeconds(1);
					mensaje_espera = false;
				pregunta3_habilitada=true;
				Pregunta2.SetActive(false);	
				Pregunta3.SetActive(true);		
				
			} 
		}
		
		if(pregunta3_habilitada){
			Pregunta3.SetActive(true);
			if( Input.GetKeyDown("2") || Input.GetKeyDown("3") || Input.GetKeyDown("4") || Input.GetKeyDown("5") || Input.GetKeyDown("6") || Input.GetKeyDown("7") || Input.GetKeyDown("8") || Input.GetKeyDown("9") || Input.GetKeyDown("1")){
				pregunta1_habilitada=false;
				pregunta2_habilitada=false;
				pregunta3_habilitada=false;
				Pregunta3.SetActive(false);	
				
				if(preguntasBuenas < 2){
					agregarBajo();
					perder();		
			
				}else{	
				
					ganar();
				
				}
								
			}
		}
		
	}
}

function agregarBajo(){
	obrero.bajosActuales += 1;
}

function OnTriggerExit(player: Collider){
	inTrigger = false;
	if(player.tag=="Player"){
		
		Pregunta1.SetActive(false);
		Pregunta2.SetActive(false);
		Pregunta3.SetActive(false);
		
	}
}

function OnGUI() {

    if(mensajeBadNews){

       	GUI.Label(Rect(Screen.width/2-100,100,200,60),"Perdiste el Parcial, caes en un bajo e intentalo de nuevo!");
		
		
    }
	if(mensajeGoodNews){
		GUI.Label(Rect(Screen.width/2-100,50,200,60),"Pasaste Calculo!!!");
	}
	if(mensaje_espera){
			GUI.Label(Rect(Screen.width/2-100,50,200,60),"Ok, Siguiente pregunta....");
	}
	

}