#pragma strict

var Pista: GameObject;
var pista4: boolean = false;
var pista5: boolean = false;
var pista6: boolean = false;
private var obrero : stadisticsPlayer;
private var parcial_fada : Rigidbody;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);
	parcial_fada = GameObject.Find("parcial_fada").GetComponent(Rigidbody);
	Pista.active=false;
}

function Update () {
	if(obrero.pistas == 6){
		parcial_fada.useGravity = true;
	}
}

function OnTriggerStay(player: Collider){
	if(player.tag=="Player"){
		Pista.active=true;
		
		if(!pista4 && Pista.name == "Canvas_pista4"){
			pista4 = true;
			obrero.pistas += 1;
		}
		if(!pista5 && Pista.name == "Canvas_pista5"){
			pista5 = true;
			obrero.pistas += 1;
		}
		if(!pista6 && Pista.name == "Canvas_pista6"){
			pista6 = true;
			obrero.pistas += 1;
		}
		
		
	}
}

function OnTriggerExit(player: Collider){
	if(player.tag=="Player"){
		Pista.active=false;
	}
}
