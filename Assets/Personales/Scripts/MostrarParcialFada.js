#pragma strict
var Pregunta1: GameObject;
var Pregunta2: GameObject;

var pregunta1_habilitada: boolean = true;
var pregunta2_habilitada: boolean = false;
private var obrero : stadisticsPlayer;
var obrerosalta : GameObject;

var inTrigger : boolean = false;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);	
	
	Pregunta1.SetActive(false);
	Pregunta2.SetActive(false);
}

function Update () {
	if(inTrigger) {
		if(pregunta1_habilitada){
			if(Input.GetKeyDown("7") || Input.GetKeyDown("3") || Input.GetKeyDown("9") ){
				
				agregarBajo();
			}
			
		}
		if(pregunta2_habilitada){
			if(Input.GetKeyDown("6") || Input.GetKeyDown("5") || Input.GetKeyDown("1") ){
				
				agregarBajo();
			}
		}
		
	}
	
}



function OnTriggerStay(player: Collider){
	
	
	
	if(player.tag=="Player"){
		
		inTrigger = true;
		
		if(pregunta1_habilitada){
			Pregunta1.SetActive(true);
			
			if(Input.GetKeyDown("0") ){
				Pregunta1.SetActive(false);
				pregunta1_habilitada=false;
				pregunta2_habilitada=true;
				Pregunta2.SetActive(true);
			}
			
		}
		if(pregunta2_habilitada){
			Pregunta2.SetActive(true);
			if(Input.GetKeyDown("8") ){
				pregunta1_habilitada=false;
				pregunta2_habilitada=false;
				Pregunta2.SetActive(false);
				obrerosalta.GetComponent(ThirdPersonController).canJump = true;
				obrero.max_pistas = 9;
				
			}
		}
		
	}
}

function agregarBajo(){
	obrero.bajosActuales += 1;
}

function OnTriggerExit(player: Collider){
	inTrigger = false;
	if(player.tag=="Player"){
		Pregunta1.SetActive(false);
		Pregunta2.SetActive(false);
	}
}