#pragma strict

var bajosActuales : float = 0.0;
var max_bajosActuales : int = 5;


var vidaActuales : float = 10.0;
var max_vidaActuales : int = 10;

var pistas : float = 0.0;
var max_pistas : int = 3;

private var barLength = 0.0;

var textura : Texture2D;
var muestra : boolean;

function Start () {
	barLength = Screen.width / 8;
}

function Update () {
	if (Input.GetKeyDown (KeyCode.P)) {
		Time.timeScale = 1.0-Time.timeScale;
		Time.fixedDeltaTime = 0.02 * Time.timeScale;
		muestra=!muestra;
	}
	
	if(bajosActuales >= max_bajosActuales){
		//abrir escena de pierde por bajo rendimiento
		CharacterDeath();
	}
	
	if(vidaActuales <= 0){
		//abrir escena de pierde por vidas
	}
	
}


function CharacterDeath()
{
	Application.LoadLevel("menu");
}

function OnGUI()
{
	
	GUI.Box(new Rect(5, 30, 100, 23), "Bajos");
	GUI.Box(new Rect(5, 55, 100, 23), "Vida");
	GUI.Box(new Rect(5, 80, 100, 23), "Pista");
	
	
	
	GUI.Box(new Rect(120, 30, barLength, 25), bajosActuales.ToString("0") + "/" + max_bajosActuales);
	GUI.Box(new Rect(120, 55, barLength, 25), vidaActuales.ToString("0") + "/" + max_vidaActuales);
	GUI.Box(new Rect(120, 80, barLength, 20), pistas.ToString("0") + "/" + max_pistas);
	
	 if(muestra){
     //GUI.Label(Rect(230,130,400,400), textura); // muestra textura
    GUI.Box(new Rect(120, 120, barLength, 23), "Pausa"); // muestra texto
    }
	
}