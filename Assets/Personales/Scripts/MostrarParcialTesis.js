#pragma strict
var Pregunta1: GameObject;


var pregunta1_habilitada: boolean = true;

private var obrero : stadisticsPlayer;
private var rampa: moverrampa;

var inTrigger : boolean = false;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);	
	rampa = GameObject.Find("LavatoryTruck").GetComponent(moverrampa);
	Pregunta1.SetActive(false);
	
}

function Update () {
	if(inTrigger) {
		if(pregunta1_habilitada){
			if(Input.GetKeyDown("9") || Input.GetKeyDown("5") || Input.GetKeyDown("1") ){
				
				agregarBajo();
			}
			
		}		
		
	}
	
}



function OnTriggerStay(player: Collider){
	if(player.tag=="Player"){
		inTrigger = true;
		
		if(pregunta1_habilitada){
			Pregunta1.SetActive(true);
			
			if(Input.GetKeyDown("4") ){
				Pregunta1.SetActive(false);
				pregunta1_habilitada=false;	
				
				moverRampa();	
				
			}
			
		}
		
		
	}
}

function agregarBajo(){
	obrero.bajosActuales += 1;
}

function OnTriggerExit(player: Collider){
	inTrigger = false;
	if(player.tag=="Player"){
		Pregunta1.SetActive(false);
		
	}
}

function moverRampa(){
	
	rampa.mover = true;
	
}