#pragma strict

var Pista: GameObject;
var pista1: boolean = false;
var pista2: boolean = false;
var pista3: boolean = false;
private var obrero : stadisticsPlayer;
private var parcial_calculo : Rigidbody;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);
	parcial_calculo = GameObject.Find("parcial_calculo").GetComponent(Rigidbody);
	Pista.active=false;
}

function Update () {
	if(obrero.pistas == 3){
		parcial_calculo.useGravity = true;
	}
}

function OnTriggerStay(player: Collider){
	if(player.tag=="Player"){
		Pista.active=true;
		
		if(!pista1 && Pista.name == "Canvas_pista1"){
			pista1 = true;
			obrero.pistas += 1;
		}
		if(!pista2 && Pista.name == "Canvas_pista2"){
			pista2 = true;
			obrero.pistas += 1;
		}
		if(!pista3 && Pista.name == "Canvas_pista3"){
			pista3 = true;
			obrero.pistas += 1;
		}
		
		
	}
}

function OnTriggerExit(player: Collider){
	if(player.tag=="Player"){
		Pista.active=false;
	}
}
