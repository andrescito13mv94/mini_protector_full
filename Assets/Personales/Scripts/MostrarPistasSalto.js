#pragma strict

var Pista: GameObject;
var pista7: boolean = false;
var pista8: boolean = false;
var pista9: boolean = false;
private var obrero : stadisticsPlayer;
private var parcial_fada : Rigidbody;

function Start () {
	obrero = GameObject.Find("Contenedor_Personaje").GetComponent(stadisticsPlayer);
	parcial_fada = GameObject.Find("parcial_tesis").GetComponent(Rigidbody);
	Pista.active=false;
}

function Update () {
	if(obrero.pistas == 9){
		parcial_fada.useGravity = true;
	}
}

function OnTriggerStay(player: Collider){
	if(player.tag=="Player"){
		Pista.active=true;
		
		if(!pista7 && Pista.name == "Canvas_pista7"){
			pista7 = true;
			obrero.pistas += 1;
		}
		if(!pista8 && Pista.name == "Canvas_pista8"){
			pista8 = true;
			obrero.pistas += 1;
		}
		if(!pista9 && Pista.name == "Canvas_pista9"){
			pista9 = true;
			obrero.pistas += 1;
		}
		
		
	}
}

function OnTriggerExit(player: Collider){
	if(player.tag=="Player"){
		Pista.active=false;
	}
}
